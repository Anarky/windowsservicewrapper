package be.anarky

import java.io.File
import java.time.LocalDateTime

fun main (args: Array<String>){

    val tenSecondsInMilliSeconds : Long = 1000 * 10
    val fileName = "C:\\dev\\output.log"
    while (true){
        var fileContent = "${LocalDateTime.now()} - Doing some work\n"
        File(fileName).appendText(fileContent)
        println(fileContent)
        Thread.sleep(tenSecondsInMilliSeconds)
    }
}

